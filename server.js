const express = require('express');
const session = require('express-session')
const bcrypt = require('bcrypt')
const saltRounds = 10;
const Redis = require('ioredis');
const jwt = require('jsonwebtoken');
const accessTokenSecret = 'youraccesstokensecret';
const redis = new Redis({
    name: "redis",
    host: "redis-master", 
    port: 6379,
    password: "OQdoUN7YANU4BB8diTGZMjxBs5M1a*HJ"
  });
const app = express();
const port = process.env.PORT || 8000;
app.use(session({
    secret: 'table flip',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true},
}))

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                console.log(err)
                return res.sendStatus(403);
            }
            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
}

app.use(express.urlencoded({
    extended: true
}))
app.use(express.json())

app.listen(port, () => {
    console.log('Server app listening on port ' + port);
});

app.get('/', (req, res) => {
    res.set('Content-Type', 'text/html');
    res.send('Hello World !!');
});

app.get('/users/:id', authenticateJWT, async (req, res) => {
    const sess = req.session
    if (req.user.email == req.params.id && sess) {
        const user = await redis.hgetall(req.params.id)
        res.json({success: true, data: user});
    } else {
        res.sendStatus(401)
    }
    
});

app.post('/login', async (req, res) => {
    try {
        const user = await redis.hgetall(req.body.email)
        if (bcrypt.compareSync(req.body.password, user.password)) {
            const accessToken = jwt.sign({username: user.username, email: user.email}, accessTokenSecret)
            const sess = req.session
            sess.accessToken = accessTokenSecret
            res.status(200).json(accessToken)
            return;
        }
        res.status(200).json({success: false, error:"Email or password incorrect"})
    } catch (error) {
        console.log(error)
        res.json({success: false, error:error.message})
        return;
    }
});

app.post('/register', async (req, res) => {
    try {
        const hash = bcrypt.hashSync(req.body.password, saltRounds)
        const data = {
            "email": req.body.email,
            "username": req.body.pseudo,
            "password": hash
        }
        await redis.hset(req.body.email, data)
        const accessToken = jwt.sign({username: req.body.username, email: req.body.email}, accessTokenSecret)
        res.json({success: true, accessToken: accessToken})
    } catch(error){
        console.log(error)
        res.json({success: false, error:error.message})
        return;
    }
    
});

app.get('/logout', (req, res) => {
    req.session.destroy(err => {
        if(err){
            return console.log(err)
        }
        res.redirect("/")
    })
});