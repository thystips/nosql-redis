FROM node:15

WORKDIR /app

COPY package.json .

RUN npm install

COPY server.js .

CMD ["npm", "start"]

EXPOSE 8000