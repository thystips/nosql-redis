# Redis

## Cluster Redis (Infra)

Pour la mise en cluster redis nous avons choisi d'utiliser le système de réplication de redis avec 1 master et 2 slaves et de rajouter redis-sentinel par dessus.

Redis est notre serveur de base de donnée qui démarre avec un master et des slaves définis et sentinel va permettre de surveiller l'activité de ces services. Dans le cas d'une panne du master sentinel va lui même élire un nouveau master permis les slaves disponibles.

Une autre solution aurait été d'utiliser redis cluster mais ce dernier est moins adapté aux environnements docker comme celui que nous utilisons pour ce TP.
Dans un environnement docker redis cluster à besoin d'utiliser le réseau de l'hôte car il ne support pas le NAT ce qui retire une couche d'isolation fournie par docker. De plus pour la création d'un cluster une commande est nécessaire ce qui n'est pas le cas avec Sentinel.

Nous avons donc un docker-compose.yml qui contient les noeuds redis ainsi que les noeuds sentinels.

Pour sécuriser notre installation nous avons défini un mot de passe en variable d'environnement, sans ce mot de passe une personne pourrait potentiellement modifier ou récupérer des données utilisateur sur notre cluster redis.

## Application

## Utilisation

Pour lancer notre application il faut définir le fichier de variables `.env.exemple`.

```bash
$ cp .env.exemple .env
```

```bash
$ docker-compose up --scale redis-sentinel=3 -d --build
```

`redis-sentinel=3` permet de lancer 3 instance de sentinel, sans la persistance il aurait été possible de faire la même chose avec les les slaves redis.

Vous pouvez aussi lancer :

```bash
$ npm run prod
```
